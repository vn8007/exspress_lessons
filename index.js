const express = require('express');
const dotenv = require('dotenv');
const generateUsers = require('./users');
const util= require('util');
const encoder = new util.TextEncoder('utf-8');

const mongoose = require('mongoose');

const User = require('./models/user.model');

dotenv.config();

const connectionString = process.env.CONNECTION_STRING;

let users = [];

const generateId = () => {
  const id = Math.floor(Math.random() * (1000 - 1)) + 1;

  return users.find((u) => u.id === id) ? generateId() : id;
};

const app = express();

app.use(express.json());

const findUsersByName = (name) =>
    users.filter(
        (user) =>
            user.profile.firstName.toLowerCase().includes(name.toLowerCase()) ||
            user.profile.lastName.toLowerCase().includes(name.toLowerCase())
    );

app.post('/users', async (req, res) => {
  const optionalKeys = ['age', 'sex', 'isMarried'];
  const requiredKeys = ['password', 'email', 'profile'];

  const filteredKeys = Object.keys(req.body).filter((k) =>
      requiredKeys.includes(k)
  );

  if (filteredKeys.length !== requiredKeys.length) {
    res
        .status(400)
        .send(`Keys ${requiredKeys.join(',')} are required!`)
        .end();
  }

  filteredKeys.forEach((k) => {
    if (req.body[k] === null || req.body === undefined) {
      res.status(400).send(`Key ${k} is required!`).end();
    }
  });

  const user = {};

  [...filteredKeys, ...optionalKeys.filter((key) => req.body[key])].forEach(
      (key) => {
        user[key] = req.body[key];
      }
  );

  user.id = generateId();

  users.push(user);
  res
      .send(
          `User with username ${user.profile.username} created! Id - ${user.id}`
      )
      .end();
});

app.use('/', async (req, res, next) => {
  const b64auth = (req.headers.authorization || '').split(' ')[1] || '';

  const first = Buffer.from(b64auth, 'base64');
  const second = first.toString();
  const third = second.split(':');

  const username = third[0];
  const password = third[1];

  const user = users.find((u) => u.profile.username === username);

  const userNotFound = !user;
  const passwordsNotMatch = user && user.password !== password;
  if (userNotFound || passwordsNotMatch) {
    res.status(401).send({
      message: userNotFound ? 'User not found!' : 'Passwords not match!',
    });

    return;
  }

  req.currentUser = user;
  next();
});

app.patch('/users/profile', async (req, res) => {
  req.currentUser.profile.firstName = req.body.firstName;

  users = [
    req.currentUser,
    ...users.filter((u) => u.id !== req.currentUser.id),
  ];

  res.send(`User with id ${req.currentUser.id} updated!`).end();
});

app.get('/users/profile', async (req, res) => {
  res.send(req.currentUser).end();
});

app.use('/users/:id', async (req, res, next) => {
  let user = users.find((u) => u.id === +req.params.id);
  if (!user) {
    res.status(404).send('User not found').end();
  }

  req.user = user;

  next();
});

app.get('/users/:id', async (req, res) => {
  res.send(req.user).end();
});

app.get('/users', async (req, res) => {
  const isQueryExist = Object.keys(req.query).length !== 0;

  const response = isQueryExist ? findUsersByName(req.query.name) : users;
  res.send(response).end();
});

app.delete('/users/:id', async (req, res) => {
  users = users.filter((u) => u.id !== req.user.id);
  res.send(`User with id ${req.user.id} delete!`).end();
});

mongoose
    .connect(connectionString, {
      useNewUrlParser: true,
    })
    .then(async () => {
      const user = new User({
        age: 21,
        password: 'Secret123',
        sex: 'MALE',
        profile: {
          username: 'cheg',
          firstName: 'Anton',
          lastName: 'Nikolenko',
          date: new Date(),
        },
        email: 'email@gmail.com',
      });

      await user.save();

      console.log('User created!');
      console.dir(user);

      app.listen(process.env.APP_PORT, () => {
        console.log(`App start on port ${process.env.APP_PORT}`);
      });
    });