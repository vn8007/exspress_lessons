const fs = require('fs');

const SEX = { MALE: 'MALE', FEMALE: 'FEMALE' };
const USER_COUNT = 20;
/* 
	User

	age:number
	password:string
	sex: 'MALE'|'FEMALE'
	profile: {
		username:string,
		firstName:string,
		lastName:string
		dateOfBirth:Date
	}
	email:string
	isMarried:boolean
	children: [ 
		User
	]

*/

const names = [
  'Harry',
  'Ross',
  'Bruce',
  'Cook',
  'Carolyn',
  'Morgan',
  'Albert',
  'Walker',
  'Randy',
  'Reed',
  'Larry',
  'Barnes',
  'Lois',
  'Wilson',
  'Jesse',
  'Campbell',
  'Ernest',
  'Rogers',
  'Theresa',
  'Patterson',
  'Henry',
  'Simmons',
  'Michelle',
  'Perry',
  'Frank',
  'Butler',
  'Shirley',
];

const lastNames = [
  'Ruth',
  'Jackson',
  'Debra',
  'Allen',
  'Gerald',
  'Harris',
  'Raymond',
  'Carter',
  'Jacqueline',
  'Torres',
  'Joseph',
  'Nelson',
  'Carlos',
  'Sanchez',
  'Ralph',
  'Clark',
  'Jean',
  'Alexander',
  'Stephen',
  'Roberts',
  'Eric',
  'Long',
  'Amanda',
  'Scott',
  'Teresa',
  'Diaz',
  'Wanda',
  'Thomas',
];

const getName = () => names[Math.floor(Math.random() * (names.length - 1))];
const getLastName = () =>
  lastNames[Math.floor(Math.random() * (names.length - 1))];

const getAge = () => Math.floor(Math.random() * (40 - 1)) + 1;
const getString = () => (Math.random() + 1).toString(36).substring(7);
const getMale = () => (Math.random() > 0.5 ? SEX.FEMALE : SEX.MALE);
const getMarried = () => Math.random() > 0.5;
const getProfile = () => ({
  username: getString(),
  firstName: getName(),
  lastName: getLastName(),
  date: new Date(),
});

const getUser = (id) => ({
  id,
  age: getAge(),
  profile: getProfile(),
  sex: getMale(),
  isMarried: getMarried(),
  password: getString(),
  email: getString(),
});

module.exports = (userCount) =>
  Array.from({ length: userCount }, (_, i) => getUser(i));
